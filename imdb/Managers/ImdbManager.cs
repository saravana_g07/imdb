﻿using imdb.Context;
using imdb.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace imdb.Managers
{
    public class ImdbManager
    {
        private static ImdbManager _instance = null;

        public static ImdbManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ImdbManager();
                }
                return _instance;
            }
        }

        public List<int> InsertMovieInfo(ITransaction appCtx, JArray movieInfoList)
        {
            try
            {
                List<Movies> movieList = new List<Movies>();
                List<MovieActors> movieActorsList = new List<MovieActors>();

                foreach (var movieObj in movieInfoList)
                {
                    #region save movie info
                    Movies movie = new Movies();
                    movie.Title = (string)movieObj["Title"];
                    movie.Plot = (string)movieObj["Plot"];
                    movie.ReleaseYear = Convert.ToInt32(movieObj["ReleaseYear"]);
                    movie.Poster = (string)movieObj["Poster"];
                    if (movieObj["newProducer"] != null)
                    {
                        movie.Producer = InsertUpdateActorProducer(appCtx, (JArray)movieObj["newProducer"])[0];
                    }
                    else
                    {
                        movie.Producer = Convert.ToInt32(movieObj["Producer"]);
                    }

                    movieList.Add(movie);

                    #endregion save movie info

                    #region save Movie actors

                    List<int> thisMovieActorsList = ((JArray)movieObj["Actors"]).Select(s => Convert.ToInt32(s)).ToList();
                    List<int> thisMovieActors = thisMovieActorsList.Where(x => x > 0).ToList();

                    if (movieObj["newActors"] != null)
                    {
                        thisMovieActors.AddRange(InsertUpdateActorProducer(appCtx, (JArray)movieObj["newActors"]));
                    }

                    appCtx.Save(movie);

                    foreach (int actorId in thisMovieActors)
                    {
                        MovieActors movieActors = new MovieActors();
                        movieActors.MovieId = movie.Id;
                        movieActors.ActorId = actorId;

                        movieActorsList.Add(movieActors);
                    }
                    #endregion save Movie actors
                }
                //appCtx.Save(movieList);
                appCtx.Save(movieActorsList);

                return movieList.Select(s => s.Id).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<int> UpdateMovieInfo(ITransaction appCtx, JArray movieInfoList)
        {
            try
            {
                List<Movies> movieList = new List<Movies>();
                List<MovieActors> movieActorsList = new List<MovieActors>();
                List<int> movieIdList = movieList.Select(s => s.Id).ToList();
                foreach (var movieObj in movieInfoList)
                {
                    movieIdList.Add(Convert.ToInt32(movieObj["id"]));
                }

                List<Movies> existingMovieInfoList = appCtx.appContext.Movie.Where(x => movieIdList.Contains(x.Id)).ToList();

                List<MovieActors> currentMovieActors = appCtx.appContext.MovieActors.Where(x => movieIdList.Contains(x.MovieId)).Select(s => s).ToList();
                appCtx.Delete(currentMovieActors);

                foreach (var movieObj in movieInfoList)
                {
                    #region save movie info
                    Movies movie = existingMovieInfoList.Where(x => x.Id == Convert.ToInt32(movieObj["id"])).FirstOrDefault();
                    movie.Title = (string)movieObj["title"];
                    movie.Plot = (string)movieObj["plot"];
                    movie.ReleaseYear = Convert.ToInt32(movieObj["releaseYear"]);
                    movie.Producer = Convert.ToInt32(movieObj["producer"]);

                    movieList.Add(movie);

                    #endregion save movie info

                    #region save Movie actors
                    List<int> thisMovieActors = ((JArray)movieObj["actors"]).Select(s => Convert.ToInt32(s)).ToList();

                    foreach (int actorId in thisMovieActors)
                    {
                        MovieActors movieActors = new MovieActors();
                        movieActors.MovieId = movie.Id;
                        movieActors.ActorId = actorId;

                        movieActorsList.Add(movieActors);
                    }
                    #endregion save Movie actors

                }
                appCtx.Save(movieList);
                appCtx.Save(movieActorsList);

                return movieIdList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<int> InsertUpdateActorProducer(ITransaction appCtx, JArray persionListObj)
        {
            try
            {
                List<int> personIdList = new List<int>();
                foreach (var personObj in persionListObj)
                {
                    personIdList.Add(Convert.ToInt32(personObj["id"]));
                }

                List<Persons> editablePersonList = appCtx.appContext.Person.Where(x => personIdList.Contains(x.Id)).ToList();
                List<Persons> personList = new List<Persons>();
                foreach (var personObj in persionListObj)
                {
                    Persons person;
                    if (Convert.ToInt32(personObj["id"]) > 0)
                    {
                        person = editablePersonList.Where(x => x.Id == Convert.ToInt32(personObj["id"])).FirstOrDefault();
                    }
                    else
                    {
                        person = new Persons();
                    }
                    person.Name = (string)personObj["name"];
                    person.Dob = Convert.ToDateTime(personObj["dob"]);
                    person.Bio = (string)personObj["bio"];
                    person.Gender = Convert.ToBoolean(personObj["gender"]);
                    person.Role = Convert.ToInt32(personObj["role"]);
                    personList.Add(person);
                }
                appCtx.Save(personList);
                return personList.Select(s => s.Id).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Tuple<List<Movies>, List<MovieActors>> GetListOfMovieInfo(ITransaction appCtx)
        {
            try
            {
                List<Movies> moviesList = appCtx.appContext.Movie.ToList();
                List<MovieActors> movieActorsList = appCtx.appContext.MovieActors.ToList();

                return new Tuple<List<Movies>, List<MovieActors>>(moviesList, movieActorsList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Persons> GetListOfActors(ITransaction appCtx)
        {
            try
            {
                return appCtx.appContext.Person.Where(x => x.Role == (int)PersonRole.Actor).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Persons> GetListOfProducers(ITransaction appCtx)
        {
            try
            {
                return appCtx.appContext.Person.Where(x => x.Role == (int)PersonRole.Producer).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
﻿using imdb.Context;
using imdb.Managers;
using imdb.Utility;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace imdb.Controllers
{
    public class ImdbController : ApiController
    {
        private Result result = new Result();

        [HttpPost]
        [Route("api/Imdb/InsertMovieInfo")]
        public Result InsertMovieInfo([FromBody]JObject jObj)
        {
            ITransaction thisTransaction = null;
            try
            {
                thisTransaction = Transaction.Instance.GetTransaction();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = ImdbManager.Instance.InsertMovieInfo(thisTransaction, (JArray)jObj["MovieList"]);
                thisTransaction.Commit();
                return result;
            }
            catch (Exception ex)
            {
                if (thisTransaction != null)
                    thisTransaction.Rollback();
                result.StatusCode = (int)HttpStatusCode.BadRequest;
                string exceptionMessage = "Message : " + ex.Message.ToString() + "    StackTrace : " + ex.StackTrace.ToString();
                if (ex.InnerException != null)
                {
                    exceptionMessage = exceptionMessage + "   InnerException : " + ex.InnerException.ToString();
                }
                result.Response = null;
                return result;
            }
        }

        [HttpPost]
        [Route("api/Imdb/UpdateMovieInfo")]
        public Result UpdateMovieInfo([FromBody]JObject jObj)
        {
            ITransaction thisTransaction = null;
            try
            {
                thisTransaction = Transaction.Instance.GetTransaction();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = ImdbManager.Instance.UpdateMovieInfo(thisTransaction, (JArray)jObj["MovieList"]);
                thisTransaction.Commit();
                return result;
            }
            catch (Exception ex)
            {
                if (thisTransaction != null)
                    thisTransaction.Rollback();
                result.StatusCode = (int)HttpStatusCode.BadRequest;
                string exceptionMessage = "Message : " + ex.Message.ToString() + "    StackTrace : " + ex.StackTrace.ToString();
                if (ex.InnerException != null)
                {
                    exceptionMessage = exceptionMessage + "   InnerException : " + ex.InnerException.ToString();
                }
                result.Response = null;
                return result;
            }
        }

        [HttpPost]
        [Route("api/Imdb/InsertUpdateActorProducer")]
        public Result InsertUpdateActorProducer([FromBody]JObject jObj)
        {
            ITransaction thisTransaction = null;
            try
            {
                thisTransaction = Transaction.Instance.GetTransaction();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = ImdbManager.Instance.InsertUpdateActorProducer(thisTransaction, (JArray)jObj["PersonList"]);
                thisTransaction.Commit();
                return result;
            }
            catch (Exception ex)
            {
                if (thisTransaction != null)
                    thisTransaction.Rollback();
                result.StatusCode = (int)HttpStatusCode.BadRequest;
                string exceptionMessage = "Message : " + ex.Message.ToString() + "    StackTrace : " + ex.StackTrace.ToString();
                if (ex.InnerException != null)
                {
                    exceptionMessage = exceptionMessage + "   InnerException : " + ex.InnerException.ToString();
                }
                result.Response = null;
                return result;
            }
        }

        [HttpGet]
        [Route("api/Imdb/GetListOfMovieInfo")]
        public Result GetListOfMovieInfo()
        {
            ITransaction thisTransaction = null;
            try
            {
                thisTransaction = Transaction.Instance.GetTransaction();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = ImdbManager.Instance.GetListOfMovieInfo(thisTransaction);
                thisTransaction.Commit();
                return result;
            }
            catch (Exception ex)
            {
                if (thisTransaction != null)
                    thisTransaction.Rollback();
                result.StatusCode = (int)HttpStatusCode.BadRequest;
                string exceptionMessage = "Message : " + ex.Message.ToString() + "    StackTrace : " + ex.StackTrace.ToString();
                if (ex.InnerException != null)
                {
                    exceptionMessage = exceptionMessage + "   InnerException : " + ex.InnerException.ToString();
                }
                result.Response = null;
                return result;
            }
        }

        [HttpGet]
        [Route("api/Imdb/GetListOfActors")]
        public Result GetListOfActors()
        {
            ITransaction thisTransaction = null;
            try
            {
                thisTransaction = Transaction.Instance.GetTransaction();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = ImdbManager.Instance.GetListOfActors(thisTransaction);
                thisTransaction.Commit();
                return result;
            }
            catch (Exception ex)
            {
                if (thisTransaction != null)
                    thisTransaction.Rollback();
                result.StatusCode = (int)HttpStatusCode.BadRequest;
                string exceptionMessage = "Message : " + ex.Message.ToString() + "    StackTrace : " + ex.StackTrace.ToString();
                if (ex.InnerException != null)
                {
                    exceptionMessage = exceptionMessage + "   InnerException : " + ex.InnerException.ToString();
                }
                result.Response = null;
                return result;
            }
        }

        [HttpGet]
        [Route("api/Imdb/GetListOfProducers")]
        public Result GetListOfProducers()
        {
            ITransaction thisTransaction = null;
            try
            {
                thisTransaction = Transaction.Instance.GetTransaction();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = ImdbManager.Instance.GetListOfProducers(thisTransaction);
                thisTransaction.Commit();
                return result;
            }
            catch (Exception ex)
            {
                if (thisTransaction != null)
                    thisTransaction.Rollback();
                result.StatusCode = (int)HttpStatusCode.BadRequest;
                string exceptionMessage = "Message : " + ex.Message.ToString() + "    StackTrace : " + ex.StackTrace.ToString();
                if (ex.InnerException != null)
                {
                    exceptionMessage = exceptionMessage + "   InnerException : " + ex.InnerException.ToString();
                }
                result.Response = null;
                return result;
            }
        }
    }
}

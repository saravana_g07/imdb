﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace imdb.Models
{
    public class Movies
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int ReleaseYear { get; set; }
        public string Plot { get; set; }
        public int Producer { get; set; }
        public string Poster { get; set; }
    }
}
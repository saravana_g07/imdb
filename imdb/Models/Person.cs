﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace imdb.Models
{
    public class Persons
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Dob { get; set; }
        public string Bio { get; set; }
        public bool Gender { get; set; }
        public int Role { get; set; }
    }
}
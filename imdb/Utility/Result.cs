﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace imdb.Utility
{
    public class Result
    {
        public int StatusCode { get; set; }
        public dynamic Response { get; set; }
    }
}
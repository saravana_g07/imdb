﻿var notify = {};
var cookie = $.cookie('session');
notify.success = function (message) {
    new PNotify({
        title: 'Success',
        text: message,
        type: 'success',
        styling: 'bootstrap3'
    });
}
notify.info = function (message) {
    new PNotify({
        title: 'For your information',
        text: message,
        type: 'info',
        styling: 'bootstrap3'
    });
}
notify.error = function (message) {
    new PNotify({
        title: 'Oops!',
        text: message,
        type: 'error',
        styling: 'bootstrap3'
    });
}

if (cookie != null && cookie != "") {
    window.location.href = '/index.html';
}
else if (window.location.href.indexOf('#') >= 0) {
    window.location.href = '/login.html';
}

function showLogin(e) {
    $('#register').attr('style', 'display:none');
    $('#forgotPassword').attr('style', 'display:none');
    $('#login').removeAttr('style');
}

function showRegister(e) {
    $('#login').attr('style', 'display:none');
    $('#forgotPassword').attr('style', 'display:none');
    $('#register').removeAttr('style');
}

function showForgotPassword(e) {
    $('#forgotPassword').removeAttr('style');
    $('#login').attr('style', 'display:none');
    $('#register').attr('style', 'display:none');
}

function loginUser(e) {
    $("#btnLogin").attr("disabled", "disabled");
    e.preventDefault();
    var data = {};
    if ($("#loginEmail").val() == "" && $("#pwd").val() == "") {
        bootbox.alert("Please enter User email and Password");
        $("#btnLogin").removeAttr("disabled");
        return false;
    }
    if ($("#loginEmail").val() == "") {
        bootbox.alert("Please enter User email");
        $("#btnLogin").removeAttr("disabled");
        return false;
    }
    if ($("#pwd").val() == "") {
        bootbox.alert("Please enter Password");
        $("#btnLogin").removeAttr("disabled");
        return false;
    }
    data.email = $("#loginEmail").val();
    data.pwd = $("#pwd").val();
    var d = new Date();
    data.timeZone = (0 - d.getTimezoneOffset());
    _validateCredential(data);
}

function _validateCredential(data) {
    try {
        $.ajax({
            url: 'api/User/Validate_Credential/',
            type: 'POST',
            cache: false,
            async: true,
            data: data,
            success: function (result) {
                $("#btnLogin").removeAttr("disabled");
                if (result.statusCode == 200 && result.response != null) {
                    _createUserCookie(result.response);
                    window.location.href = '/index.html';
                }
                else {
                    bootbox.alert("Invalid credential");
                }
            },
            error: function (error) {
                $("#btnLogin").removeAttr("disabled");
                //alert('Error occured');
            }
        })
    }
    catch (e) {
        developerErrorLog(e);
    }
}

function register(e) {
    $("#btnRegister").attr("disabled", "disabled");
    e.preventDefault();
    if ($("#reg_Name").val() == "") {
        bootbox.alert("Please enter user name");
        $("#btnRegister").removeAttr("disabled");
        return false;
    }
    if ($("#reg_Email").val() == "") {
        bootbox.alert("Please enter user email");
        $("#btnRegister").removeAttr("disabled");
        return false;
    }
    if ($("#reg_Pwd").val() == "") {
        bootbox.alert("Please enter password");
        $("#btnRegister").removeAttr("disabled");
        return false;
    }
    if ($("#reg_ConfirmPwd").val() == "") {
        bootbox.alert("Please enter confirmation password");
        $("#btnRegister").removeAttr("disabled");
        return false;
    }
    if ($("#reg_Pwd").val() !== $("#reg_ConfirmPwd").val()) {
        bootbox.alert("password and confirmation password should match");
        $("#btnRegister").removeAttr("disabled");
        return false;
    }
    var data = {};
    data.name = $("#reg_Name").val();
    data.email = $("#reg_Email").val();
    data.password = $("#reg_Pwd").val();
    _userRegistartion(data);
}

function _userRegistartion(data) {
    try {
        $.ajax({
            url: 'api/User/userRegistartion/',
            type: 'POST',
            cache: false,
            async: true,
            data: data,
            success: function (result) {
                $("#btnRegister").removeAttr("disabled");
                if (result.statusCode == 200) {
                    if (result.response != 1) {
                        bootbox.alert("Registration failed, Please contact your administrator");
                        return false;
                    }
                    notify.success("Registered successfully.");
                    window.location.href = '/login.html';
                    clearRegistrationForm();
                }
            },
            error: function (error) {
                $("#btnRegister").removeAttr("disabled");
                //alert('Error occured');
            }
        })
    }
    catch (e) {
        developerErrorLog(e);
    }
}

function _createUserCookie(data) {
    try {
        $.cookie('session', data.sessionId);
        $.cookie('userId', data.userInfo.id);
        $.cookie('userName', data.userInfo.name);
        $.cookie('role', data.userInfo.role);
        $.cookie('loggedinTime', data.loggedinTime);
    }
    catch (e) {
        developerErrorLog(e);
    }
}

function clearRegistrationForm() {
    $("#reg_Name")[0].value = "";
    $("#reg_Email")[0].value = "";
    $("#reg_Pwd")[0].value = "";
    $("#reg_ConfirmPwd")[0].value = "";
}

function ForgotPassword(e) {
    $("#btnForgotPassword").attr("disabled", "disabled");
    e.preventDefault();

    try {
        if ($("#userEmail").val() == "") {
            bootbox.alert("Please enter the email address");
            $("#btnForgotPassword").removeAttr("disabled");
            return false;
        }
        var data = {};
        data.email = $("#userEmail").val();
        $.ajax({
            url: 'api/User/ForgotPassword/',
            type: 'POST',
            cache: false,
            async: true,
            data: data,
            success: function (result) {
                if (result.statusCode == 200) {
                }
                else {
                    bootbox.alert("Error occured, Please contact your administrator.");
                }
                $("#userEmail").val() = "";
                $("#btnForgotPassword").removeAttr("disabled");
            },
            error: function (error) {
                //alert('Error occured');
                $("#userEmail").val() = "";
                $("#btnForgotPassword").removeAttr("disabled");
            }
        })
        bootbox.alert("If you have an account you will get a temporary password , Please check your email to reset the password.");
        $("#userEmail")[0].value = "";
    }
    catch (ex) {
        developerErrorLog(ex);
    }
}
$(document).keypress(function (e) {
    if (e.which == 13) {
        if ($("#login")[0].style.display == "") {
            $("#btnLogin").trigger("click");
            return;
        }
        if ($("#register")[0].style.display == "") {
            $("#btnRegister").trigger("click");
            return;
        }
        if ($("#forgotPassword")[0].style.display == "") {
            $("#btnForgotPassword").trigger("click");
            return;
        }
    }
});
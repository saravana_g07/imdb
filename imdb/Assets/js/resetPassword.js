﻿//notify.success = function (message) {
//    new PNotify({
//        title: 'Success',
//        text: message,
//        type: 'success',
//        styling: 'bootstrap3'
//    });
//}
//notify.info = function (message) {
//    new PNotify({
//        title: 'For your information',
//        text: message,
//        type: 'info',
//        styling: 'bootstrap3'
//    });
//}
//notify.error = function (message) {
//    new PNotify({
//        title: 'Oops!',
//        text: message,
//        type: 'error',
//        styling: 'bootstrap3'
//    });
//}
clearForm();
function _resetPassword(e) {
    try {
        e.preventDefault();
        var data = {};
        if ($("#reg_Email").val() == "" || $("#old_Pwd").val() == "" || $("#new_Pwd").val() == "" || $("#new_ConfirmPwd").val() == "") {
            bootbox.alert("Please enter all the field values");
            return false;
        }
        if ($("#new_Pwd").val() !== $("#new_ConfirmPwd").val()) {
            bootbox.alert("New password and Confirmation new password should be same!");
            return false;
        }

        data.email = $("#reg_Email").val();
        data.oldPwd = $("#old_Pwd").val();
        data.newPwd = $("#new_Pwd").val();

        $.ajax({
            url: 'api/User/ResetPassword/',
            type: 'POST',
            cache: false,
            async: true,
            data: data,
            success: function (result) {
                if (result.statusCode == 200 && result.response != 0) {
                    bootbox.alert("Your password has been reset.");
                    clearForm();
                    setTimeout(function () {
                        window.location.href = '/login.html';
                    }, 2000);
                }
                else {
                    bootbox.alert("Invalid username or current password");
                }
            },
            error: function (error) {
                //alert('Error occured');
            }

        })
    }
    catch (ex) {
        developerErrorLog(ex);
    }
}

function showLogin(e) {
    e.preventDefault();
    window.location.href = '/login.html';
}

function clearForm() {
    $("#reg_Email")[0].value = "";
    $("#old_Pwd")[0].value = "";
    $("#new_Pwd")[0].value = "";
    $("#new_ConfirmPwd")[0].value = "";
}
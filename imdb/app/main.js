﻿(function (ng, app) {
    "use strict";
    function appCtrl($scope, $timeout, ImdbService) {

        /********************* Initialize base objects & components **************************/

        InitializePage();

        var PersonRole = {
            Actor: 1,
            Producer: 2
        }
        function InitializePage() {
            $timeout(function () {
                $("#AddMovieForm").attr("style", "display:None;");
                $("#AddProducerForm").attr("style", "display:None;");
                $("#AddActorForm").attr("style", "display:None;");
            }, 200);
            $('.datepicker').datetimepicker({
                format: 'YYYY-MM-DD'
            });
            $('.datepicker').keyup(function () {
                this.value = '';
            });
            $scope.viewId = 0;
            $timeout(function () {
                $(".menuList").removeClass("active");
                $("#moviesListli").addClass("active");
            }, 500);
            $scope.loadView = function (view, menuId) {
                $scope.viewId = view;
                LoadImdbData(view);
                $(".menuList").removeClass("active");
                $("#" + menuId).addClass("active");
            }


            $scope.formatDate = function (date) {
                var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;

                return [year, month, day].join('-');
            }

            $scope.YearList = [];
            for (var i = 1970; i <= (new Date().getFullYear()) + 1; i++) {
                $scope.YearList.push(i);
            }
        }

        /********************* Load page data **************************/

        $scope.ClearNewMovieInfo = function ($event) {
            $scope.newmovieTitle = "";
            $scope.newmoviePlot = "";
            $scope.newmovieReleaseYear = new Date().getFullYear();
            $scope.newmovieProducer = "";
            $scope.newmovieActors = [];
            $scope.ExternalActorsList = [];
            $scope.ExternalProducersList = [];
            $("#attchPreview").attr('style', 'display:none;');
            $("#fileName")[0].value = ""

            if ($event != undefined) {
                $event.preventDefault();
            }
        }

        $scope.ClearNewActorInfo = function ($event) {
            $scope.newActor = {};
            $scope.newActorName = "";
            $scope.newActorDob = "";
            $scope.newActorGender = "";
            $scope.newActorBio = "";
            if ($event != undefined) {
                $event.preventDefault();
            }
        }

        $scope.ClearNewProducerInfo = function ($event) {
            $scope.newProducer = {};
            $scope.newProducerName = "";
            $scope.newProducerDob = "";
            $scope.newProducerGender = "";
            $scope.newProducerBio = "";
            if ($event != undefined) {
                $event.preventDefault();
            }
        }

        LoadImdbData($scope.viewId);

        function GetListOfProducers() {
            ImdbService.GetListOfProducers().then(function (result) {
                if (result.response != null) {
                    $scope.ProducersList = result.response;
                }
            })
        }
        function GetListOfActors() {
            ImdbService.GetListOfActors().then(function (result) {
                if (result.response != null) {
                    $scope.ActorsList = result.response;
                }
            })
        }
        function GetListOfMovieInfo() {
            ImdbService.GetListOfMovieInfo().then(function (result) {
                if (result.response != null) {
                    $scope.MovieList = result.response.item1;
                    $scope.MovieActorsList = result.response.item2;
                }
            })
        }
        function LoadImdbData(viewId) {
            GetListOfMovieInfo();
            GetListOfActors();
            GetListOfProducers();
            switch (viewId) {
                case 0:
                    $("#AddMovieForm").attr("style", "display:None;");
                    $scope.ClearNewMovieInfo();
                    break;
                case 1:
                    $("#AddActorForm").attr("style", "display:None;");
                    $scope.ClearNewActorInfo();
                    break;
                case 2:
                    $("#AddProducerForm").attr("style", "display:None;");
                    $scope.ClearNewProducerInfo();
                    break;
            }
        }

        /********************* Insert and update actors & Producers **************************/

        $scope.SaveActor = function ($event) {

            $scope.newActorDob = $("#newActorDob")[0].value;
            if ($scope.newActorName == "" || $scope.newActorName == null) {
                bootbox.alert("Please enter actor name!");
                if ($event != undefined) {
                    $event.preventDefault();
                }
                return;
            }
            else if ($scope.newActorDob == "" || $scope.newActorDob == null) {
                bootbox.alert("Please enter actor's date of birth!");
                if ($event != undefined) {
                    $event.preventDefault();
                }
                return;
            }
            else if (new Date($scope.newActorDob) > new Date()) {
                bootbox.alert("Date of birth cannot be a future date!");
                if ($event != undefined) {
                    $event.preventDefault();
                }
                return;
            }
            else if ($scope.newActorGender == "" || $scope.newActorGender == null) {
                bootbox.alert("Please select actor's gender!");
                if ($event != undefined) {
                    $event.preventDefault();
                }
                return;
            }

            $scope.newActor = {};
            $scope.newActor.name = $scope.newActorName;
            $scope.newActor.dob = $scope.newActorDob;
            $scope.newActor.gender = $scope.newActorGender;
            $scope.newActor.bio = $scope.newActorBio;
            $scope.newActor.role = PersonRole.Actor;

            var PersonList = [];
            PersonList.push($scope.newActor);

            ImdbService.InsertUpdateActorProducer(PersonList).then(function (result) {
                if (result.response != null) {
                    notify.success("Actor added successfully");
                    $scope.ClearNewActorInfo();
                    GetListOfActors();
                }
                else {
                    notify.error("Failed to add actor");
                }
            })

            if ($event != undefined) {
                $event.preventDefault();
            }
        }

        $scope.SaveProducer = function ($event) {
            $scope.newProducerDob = $("#newProducerDob")[0].value;
            if ($scope.newProducerName == "" || $scope.newProducerName == null) {
                bootbox.alert("Please enter producer name!");
                $event.preventDefault();
                return;
            }
            else if ($scope.newProducerDob == "" || $scope.newProducerDob == null) {
                bootbox.alert("Please enter producer's date of birth!");
                $event.preventDefault();
                return;
            }
            else if (new Date($scope.newActorDob) > new Date()) {
                bootbox.alert("Date of birth cannot be a future date!");
                $event.preventDefault();
                return;
            }
            else if ($scope.newProducerGender == "" || $scope.newProducerGender == null) {
                bootbox.alert("Please select producer's gender!");
                if ($event != undefined) {
                    $event.preventDefault();
                }
                return;
            }
            $scope.newProducer = {};
            $scope.newProducer.name = $scope.newProducerName;
            $scope.newProducer.dob = $scope.newProducerDob;
            $scope.newProducer.gender = $scope.newProducerGender;
            $scope.newProducer.bio = $scope.newProducerBio;
            $scope.newProducer.role = PersonRole.Producer;

            var PersonList = [];
            PersonList.push($scope.newProducer);

            ImdbService.InsertUpdateActorProducer(PersonList).then(function (result) {
                if (result.response != null) {
                    notify.success("Producer added successfully");
                    $scope.ClearNewProducerInfo();
                    GetListOfProducers();
                }
                else {
                    notify.error("Failed to add producer");
                }
            })

            if ($event != undefined) {
                $event.preventDefault();
            }
        }

        $scope.EditPerson = function (personInfo) {
            $scope.EditPersonObject = {};
            $scope.EditPersonObject.Id = personInfo.id;
            $scope.EditPersonObject.name = personInfo.name;
            $scope.EditPersonObject.dob = $scope.formatDate(personInfo.dob);
            $scope.EditPersonObject.gender = personInfo.gender.toString();
            $scope.EditPersonObject.bio = personInfo.bio;
            $scope.EditPersonObject.role = personInfo.role;

            $("#EditPerson").modal("show");
        }

        $scope.UpdatePerson = function () {
            if ($scope.EditPersonObject.name == "" || $scope.EditPersonObject.name == null) {
                bootbox.alert("Please enter actor name!");
                return;
            }
            else if ($scope.EditPersonObject.dob == "" || $scope.EditPersonObject.dob == null) {
                bootbox.alert("Please enter actor's date of birth!");
                return;
            }
            else if (new Date($scope.EditPersonObject.dob) > new Date()) {
                bootbox.alert("Date of birth cannot be a future date!");
                return;
            }
            else if ($scope.EditPersonObject.gender == "" || $scope.EditPersonObject.gender == null) {
                bootbox.alert("Please select actor's gender!");
                return;
            }
            var PersonList = [];
            $scope.EditPersonObject.id = $scope.EditPersonObject.Id;
            $scope.EditPersonObject.dob = $("#EditPersonObjectDob")[0].value;
            PersonList.push($scope.EditPersonObject);
            ImdbService.InsertUpdateActorProducer(PersonList).then(function (result) {
                if (result.response != null) {
                    notify.success("updated successfully");
                    if (PersonList[0].role == PersonRole.Producer) {
                        GetListOfProducers();
                    }
                    if (PersonList[0].role == PersonRole.Actor) {
                        GetListOfActors();
                    }
                    $scope.EditPersonObject = {};
                    $("#EditPerson").modal("hide");
                }
                else {
                    notify.error("Failed to update");
                }
            })
        }


        /********************* Insert and update MovieInfo **************************/

        $scope.newmovie = {};

        $scope.SaveMovieInfo = function ($event) {
            if ($scope.newmovieTitle == "" || $scope.newmovieTitle == null) {
                bootbox.alert("Please enter movie name");
                if ($event != undefined) {
                    $event.preventDefault();
                }
                return;
            }
            else if ($scope.newmoviePlot == "" || $scope.newmoviePlot == null) {
                bootbox.alert("Please enter movie plot");
                if ($event != undefined) {
                    $event.preventDefault();
                }
                return;
            }
            else if ($scope.newmovieReleaseYear == "" || $scope.newmovieReleaseYear == null) {
                bootbox.alert("Please choose movie releaseYear");
                if ($event != undefined) {
                    $event.preventDefault();
                }
                return;
            }
            else if ($scope.newmovieProducer == "" || $scope.newmovieProducer == null) {
                bootbox.alert("Please choose movie producer");
                if ($event != undefined) {
                    $event.preventDefault();
                }
                return;
            }
            else if ($scope.newmovieActors == null || $scope.newmovieActors.length == 0) {
                bootbox.alert("Please choose movie actor(s)");
                if ($event != undefined) {
                    $event.preventDefault();
                }
                return;
            }
            else if ($("#fileName")[0].value == "" || $("#attchPreview")[0].currentSrc == null || $("#attchPreview")[0].currentSrc == "") {
                bootbox.alert("Please choose poster as image");
                if ($event != undefined) {
                    $event.preventDefault();
                }
                return;
            }

            $scope.newmovie.Title = $scope.newmovieTitle;
            $scope.newmovie.Plot = $scope.newmoviePlot;
            $scope.newmovie.ReleaseYear = $scope.newmovieReleaseYear;
            $scope.newmovie.Poster = $("#attchPreview")[0].currentSrc;
            $scope.newmovie.Producer = $scope.newmovieProducer;
            $scope.newmovie.Actors = $scope.newmovieActors;

            if ($scope.newmovie.Producer < 0) {
                var newProd = $.grep($scope.ExternalProducersList, function (e) {
                    return e.id == $scope.newmovie.Producer
                })
                if (newProd.length > 0) {
                    if ($scope.newmovie.newProducer == null || $scope.newmovie.newProducer == undefined) {
                        $scope.newmovie.newProducer = [];
                    }
                    $scope.newmovie.newProducer.push(newProd[0])
                }
            }
            if ($scope.newmovieActors.length > 0) {
                angular.forEach($scope.newmovie.Actors, function (actr) {
                    if (actr < 0) {
                        var newactr = $.grep($scope.ExternalActorsList, function (e) {
                            return e.id == actr
                        })
                        if (newactr.length > 0) {
                            if ($scope.newmovie.newActors == null || $scope.newmovie.newActors == undefined) {
                                $scope.newmovie.newActors = [];
                            }
                            $scope.newmovie.newActors.push(newactr[0])
                        }
                    }
                });
            }


            var movieList = [];
            movieList.push($scope.newmovie);

            ImdbService.InsertMovieInfo(movieList).then(function (result) {
                if (result.response != null) {
                    notify.success("New movie added successfully");
                    $scope.ClearNewMovieInfo();
                    GetListOfMovieInfo();
                    GetListOfProducers();
                    GetListOfActors();
                }
                else {
                    notify.error("Failed to add new movie");
                }
            })

            if ($event != undefined) {
                $event.preventDefault();
            }
        }


        $scope.GetMovieActors = function (movieData) {
            if ($scope.MovieActorsList != null && $scope.ActorsList != null) {
                var actorNameList = [];
                var actors = $.grep($scope.MovieActorsList, function (e) {
                    return e.movieId == movieData.id
                })
                for (var i = 0; i < actors.length; i++) {
                    var actorName = $.grep($scope.ActorsList, function (e) {
                        return e.id == actors[i].actorId
                    })
                    if (actorName != null && actorName.length > 0) {
                        actorNameList.push(actorName[0].name);
                    }
                }

                return actorNameList.join(', ');
            }
        }

        $scope.GetMovieProducer = function (movieData) {
            if ($scope.ProducersList != null) {
                var producerName = $.grep($scope.ProducersList, function (e) {
                    return e.id == movieData.producer
                })
                if (producerName != null && producerName.length > 0) {
                    return producerName[0].name;
                }
            }
        }

        $scope.EditMovie = function (MovieInfo) {
            $scope.EditMovieObject = {};
            $scope.EditMovieObject.id = MovieInfo.id;
            $scope.EditMovieObject.title = MovieInfo.title;
            $scope.EditMovieObject.plot = MovieInfo.plot;
            $scope.EditMovieObject.releaseYear = MovieInfo.releaseYear.toString();
            $scope.EditMovieObject.producer = MovieInfo.producer.toString();
            $scope.EditMovieObject.actors = [];

            var actors = $.grep($scope.MovieActorsList, function (e) {
                return e.movieId == MovieInfo.id
            })
            for (var i = 0; i < actors.length; i++) {
                var actorName = $.grep($scope.ActorsList, function (e) {
                    return e.id == actors[i].actorId
                })
                if (actorName != null && actorName.length > 0) {
                    $scope.EditMovieObject.actors.push(actorName[0].id.toString());
                }
            }

            $("#EditMovie").modal("show");
        }

        $scope.UpdateMovieInfo = function () {
            if ($scope.EditMovieObject.title == "" || $scope.EditMovieObject.title == null) {
                bootbox.alert("Please enter movie name");
                return;
            }
            else if ($scope.EditMovieObject.plot == "" || $scope.EditMovieObject.plot == null) {
                bootbox.alert("Please enter movie plot");
                return;
            }
            else if ($scope.EditMovieObject.releaseYear == "" || $scope.EditMovieObject.releaseYear == null) {
                bootbox.alert("Please choose movie releaseYear");
                return;
            }
            else if ($scope.EditMovieObject.producer == "" || $scope.EditMovieObject.producer == null) {
                bootbox.alert("Please choose movie producer");
                return;
            }
            else if ($scope.EditMovieObject.actors == null || $scope.EditMovieObject.actors.length == 0) {
                bootbox.alert("Please choose movie actor(s)");
                return;
            }

            var MovieList = [];
            MovieList.push($scope.EditMovieObject);
            ImdbService.UpdateMovieInfo(MovieList).then(function (result) {
                if (result.response != null) {
                    notify.success("updated successfully");
                    $scope.EditMovieObject = {};
                    $("#EditMovie").modal("hide");
                    GetListOfMovieInfo();
                }
                else {
                    notify.error("Failed to update");
                }
            })
        }

        $scope.EditAllActors = function () {
            $.grep($scope.ActorsList, function (e) {
                return (
                    e.gender = e.gender.toString()
                )
            })
            $("#EditAllActors").modal("show");
            $('.datepicker').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        }

        $scope.EditAllProducers = function () {
            $.grep($scope.ProducersList, function (e) {
                return (
                    e.gender = e.gender.toString()
                )
            })
            $("#EditAllProducers").modal("show");
            $('.datepicker').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        }

        $scope.UpdateAllActors = function () {
            $.grep($scope.ActorsList, function (e) {
                e.dob = $("#actorDob_" + e.id)[0].value;
            })
            ImdbService.InsertUpdateActorProducer($scope.ActorsList).then(function (result) {
                if (result.response != null) {
                    notify.success("updated successfully");
                    GetListOfActors();
                    $("#EditAllActors").modal("hide");
                }
                else {
                    notify.error("Failed to update");
                }
            })
        }
        $scope.UpdateAllProducers = function () {
            $.grep($scope.ProducersList, function (e) {
                e.dob = $("#producerDob_" + e.id)[0].value;
            })
            ImdbService.InsertUpdateActorProducer($scope.ProducersList).then(function (result) {
                if (result.response != null) {
                    notify.success("updated successfully");
                    GetListOfProducers();
                    $("#EditAllProducers").modal("hide");
                }
                else {
                    notify.error("Failed to update");
                }
            })
        }

        $scope.EditAllMovies = function () {
            $.grep($scope.MovieList, function (e) {
                return (
                    e.releaseYear = e.releaseYear.toString(),
                    e.producer = e.producer.toString()
                )
            })
            for (var j = 0; j < $scope.MovieList.length; j++) {
                $scope.MovieList[j].actors = [];
                var actors = $.grep($scope.MovieActorsList, function (e) {
                    return e.movieId == $scope.MovieList[j].id
                })
                for (var i = 0; i < actors.length; i++) {
                    var actorName = $.grep($scope.ActorsList, function (e) {
                        return e.id == actors[i].actorId
                    })
                    if (actorName != null && actorName.length > 0) {
                        $scope.MovieList[j].actors.push(actorName[0].id.toString());
                    }
                }
            }

            $("#EditAllMovies").modal("show");
        }

        $scope.UpdateAllMovies = function () {

            ImdbService.UpdateMovieInfo($scope.MovieList).then(function (result) {
                if (result.response != null) {
                    notify.success("updated successfully");
                    GetListOfMovieInfo();
                    $("#EditAllMovies").modal("hide");
                }
                else {
                    notify.error("Failed to update");
                }
            })
        }

        $scope.AddExternalActor = function () {
            $scope.ExternalActorObject = {};
            $("#AddExternalActor").modal("show");
        }

        $scope.ExternalActorsList = [];
        $scope.SaveExternalActor = function ($event) {

            $scope.ExternalActorObject.dob = $("#externalActorBob")[0].value;
            if ($scope.ExternalActorObject.name == "" || $scope.ExternalActorObject.name == null) {
                bootbox.alert("Please enter actor name!");
                if ($event != undefined) {
                    $event.preventDefault();
                }
                return;
            }
            else if ($scope.ExternalActorObject.dob == "" || $scope.ExternalActorObject.dob == null) {
                bootbox.alert("Please enter actor's date of birth!");
                if ($event != undefined) {
                    $event.preventDefault();
                }
                return;
            }
            else if (new Date($scope.ExternalActorObject.dob) > new Date()) {
                bootbox.alert("Date of birth cannot be a future date!");
                if ($event != undefined) {
                    $event.preventDefault();
                }
                return;
            }
            else if ($scope.ExternalActorObject.gender == "" || $scope.ExternalActorObject.gender == null) {
                bootbox.alert("Please select actor's gender!");
                if ($event != undefined) {
                    $event.preventDefault();
                }
                return;
            }

            $scope.ExternalActorObject.id = 0 - parseInt($scope.ActorsList.length);
            $scope.ExternalActorObject.role = PersonRole.Actor;
            var _ExternalActorObject = angular.copy($scope.ExternalActorObject)
            $scope.ActorsList.push(_ExternalActorObject);
            $scope.ExternalActorsList.push(_ExternalActorObject);
            $scope.newmovieActors.push(_ExternalActorObject.id.toString());
            $("#AddExternalActor").modal("hide");
        }

        $scope.AddExternalProducer = function () {
            $scope.ExternalProducerObject = {};
            $("#AddExternalProducer").modal("show");
        }

        $scope.ExternalProducersList = [];
        $scope.SaveExternalProducer = function ($event) {

            $scope.ExternalProducerObject.dob = $("#externalProducerBob")[0].value;
            if ($scope.ExternalProducerObject.name == "" || $scope.ExternalProducerObject.name == null) {
                bootbox.alert("Please enter producer name!");
                if ($event != undefined) {
                    $event.preventDefault();
                }
                return;
            }
            else if ($scope.ExternalProducerObject.dob == "" || $scope.ExternalProducerObject.dob == null) {
                bootbox.alert("Please enter producer's date of birth!");
                if ($event != undefined) {
                    $event.preventDefault();
                }
                return;
            }
            else if (new Date($scope.ExternalProducerObject.dob) > new Date()) {
                bootbox.alert("Date of birth cannot be a future date!");
                if ($event != undefined) {
                    $event.preventDefault();
                }
                return;
            }
            else if ($scope.ExternalProducerObject.gender == "" || $scope.ExternalProducerObject.gender == null) {
                bootbox.alert("Please select producer's gender!");
                if ($event != undefined) {
                    $event.preventDefault();
                }
                return;
            }

            $scope.ExternalProducerObject.id = 0 - parseInt($scope.ProducersList.length);
            $scope.ExternalProducerObject.role = PersonRole.Producer;
            var _ExternalProducerObject = angular.copy($scope.ExternalProducerObject)
            $scope.ProducersList.push(_ExternalProducerObject);
            $scope.ExternalProducersList.push(_ExternalProducerObject);
            $scope.newmovieProducer = _ExternalProducerObject.id.toString();
            $("#AddExternalProducer").modal("hide");
        }


        $scope.previewFile = function () {
            var preview = document.querySelector('img');
            var file = document.querySelector('input[type=file]').files[0];
            var reader = new FileReader();

            reader.addEventListener("load", function () {
                preview.src = reader.result;
            }, false);

            if (file) {
                reader.readAsDataURL(file);
                $("#attchPreview").attr("style", "display:block;")
            }
        }
    }
    app.controller('appCtrl', ['$scope', '$timeout', 'ImdbService', appCtrl]);

    function ImdbService($http, $q) {
        return ({
            GetListOfActors: GetListOfActors,
            GetListOfProducers: GetListOfProducers,
            GetListOfMovieInfo: GetListOfMovieInfo,
            InsertUpdateActorProducer: InsertUpdateActorProducer,
            InsertMovieInfo: InsertMovieInfo,
            UpdateMovieInfo: UpdateMovieInfo,
            //DeletePerson: DeletePerson

        });

        function GetListOfActors() {
            var request = $http({
                method: "get",
                url: "api/Imdb/GetListOfActors",
                params: {
                    action: "Get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetListOfProducers() {
            var request = $http({
                method: "get",
                url: "api/Imdb/GetListOfProducers",
                params: {
                    action: "Get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetListOfMovieInfo() {
            var request = $http({
                method: "get",
                url: "api/Imdb/GetListOfMovieInfo",
                params: {
                    action: "Get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertUpdateActorProducer(data) {
            var request = $http({
                method: "post",
                url: "api/Imdb/InsertUpdateActorProducer",
                params: {
                    action: "post"
                },
                data: { PersonList: data }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertMovieInfo(data) {
            var request = $http({
                method: "post",
                url: "api/Imdb/InsertMovieInfo",
                params: {
                    action: "post"
                },
                data: { MovieList: data }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateMovieInfo(data) {
            var request = $http({
                method: "post",
                url: "api/Imdb/UpdateMovieInfo",
                params: {
                    action: "post"
                },
                data: { MovieList: data }
            });
            return (request.then(handleSuccess, handleError));
        }

        //function DeletePerson(id) {
        //    var request = $http({
        //        method: "get",
        //        url: "api/Imdb/DeletePerson" / +id,
        //        params: {
        //            action: "Get"
        //        }
        //    });
        //    return (request.then(handleSuccess, handleError));
        //}

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);
        }
    }
    app.service("ImdbService", ['$http', '$q', ImdbService]);

})(angular, app);
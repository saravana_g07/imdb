﻿using imdb.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace imdb.Context
{
    public class AppContext : DbContext
    {

        public AppContext()
            : base("name=DefaultConnection")
        {
            Database.SetInitializer<AppContext>(null);
        }
        public DbSet<Persons> Person { get; set; }
        public DbSet<Movies> Movie { get; set; }
        public DbSet<MovieActors> MovieActors { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace imdb.Context
{
    public interface ITransaction : IDisposable
    {
        AppContext appContext { get; set; }
        DbContextTransaction currentTransaction { get; set; }

        ITransaction GetTransaction();
        void Commit();
        void Save(dynamic obj);
        void Delete(dynamic obj);
        void Rollback();
    }
}
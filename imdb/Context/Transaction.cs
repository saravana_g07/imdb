﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace imdb.Context
{
    public class Transaction : ITransaction, IDisposable
    {
        private static ITransaction _instance;

        public static ITransaction Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Transaction();
                }
                return _instance;
            }
        }

        public AppContext appContext { get; set; }
        public DbContextTransaction currentTransaction { get; set; }

        public ITransaction GetTransaction()
        {
            Transaction thisTransaction = null;
            thisTransaction = new Transaction();
            thisTransaction.appContext = new AppContext();
            thisTransaction.currentTransaction = thisTransaction.appContext.Database.BeginTransaction();
            return thisTransaction;
        }

        public void Save(dynamic obj)
        {
            try
            {
                if (obj.GetType().Name.Contains("List"))
                {
                    foreach (var o in obj)
                    {
                        handleSave(o);
                    }
                }
                else
                {
                    handleSave(obj);
                }
                this.appContext.SaveChanges();
            }
            catch (Exception ex)
            {
                Rollback();
                throw ex;
            }
        }

        private void handleSave(dynamic obj)
        {
            try
            {
                if (obj.Id == 0)
                {
                    this.appContext.Set(obj.GetType()).Add(obj);
                }
                else
                {
                    this.appContext.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveList(List<dynamic> objList)
        {
            try
            {
                foreach (dynamic obj in objList)
                {
                    if (obj.Id == 0)
                    {
                        this.appContext.Entry(obj).State = System.Data.Entity.EntityState.Added;
                    }
                    else
                    {
                        this.appContext.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                    }
                }
                this.appContext.SaveChanges();
            }
            catch (Exception ex)
            {
                Rollback();
                throw ex;
            }
        }

        public void Delete(dynamic obj)
        {
            try
            {
                if (obj.GetType().Name.Contains("List"))
                {
                    foreach (var o in obj)
                    {
                        HandleDelete(o);
                    }
                }
                else
                {
                    HandleDelete(obj);
                }
            }
            catch (Exception ex)
            {
                Rollback();
                throw ex;
            }
        }

        private void HandleDelete(dynamic obj)
        {
            try
            {
                this.appContext.Entry(obj).State = System.Data.Entity.EntityState.Deleted;
                this.appContext.SaveChanges();
            }
            catch (Exception ex)
            {
                Rollback();
                throw ex;
            }
        }

        public void DeleteList(List<dynamic> objList)
        {
            try
            {
                foreach (dynamic obj in objList)
                {
                    this.appContext.Entry(obj).State = System.Data.Entity.EntityState.Deleted;
                }
                this.appContext.SaveChanges();
            }
            catch (Exception ex)
            {
                Rollback();
                throw ex;
            }
        }

        public void Commit()
        {
            try
            {
                this.currentTransaction.Commit();
                this.currentTransaction.Dispose();
            }
            catch
            {
                Rollback();
            }
        }

        public void Rollback()
        {
            try
            {
                this.currentTransaction.Rollback();
                this.currentTransaction.Dispose();
            }
            catch
            {
            }
        }

        public void Dispose()
        {
        }
    }
}